"""File for testing a trained vae to look at the performance

Author: Christian S. Hådem

"""
import os, sys
import numpy as np
from torchvision import transforms
from carla_dataset import CarlaDataset
from carla_vae import CarlaVae
from vae import VaeAgent

latent_vector_size = 32
transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.ToTensor()
])

if not os.path.exists('model_data/vae'):
    if not os.path.exists('model_data/dataset'):
        print("vae or dataset is missing")
        sys.exit()

dataset = CarlaDataset(transform=transform)
dataset.load()

vae = CarlaVae(latent_vector_size)
vae_agent = VaeAgent(vae)
vae_agent.load()

indicies = np.random.randint(low=0, high=len(dataset), size=10)
vae_agent.print_result(dataset, indicies, to_file=True, filename='vae_test')