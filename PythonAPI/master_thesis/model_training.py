"""This file loads up a vae and starts training a TD3 model on Carla

Author: Christian S. Hådem
"""

# ----------------------------------------------------------------------------------------------- #
# Imports                                                                                         #
# ----------------------------------------------------------------------------------------------- #
import os, sys, timeit
from torchvision import transforms
from carla_dataset import CarlaDataset
from carla_vae import CarlaVae, CarlaVae128
from carla_env import CarlaEnv
from vae import VaeAgent
from vae_gym_wrapper import VaeWrapper
from TD3 import TD3

# ----------------------------------------------------------------------------------------------- #
# Parameters                                                                                      #
# ----------------------------------------------------------------------------------------------- #
ip = 'localhost'
port = 2000
seed = 1
latent_vector_size = 32
training_timesteps = 100000
transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.ToTensor()
])

# ----------------------------------------------------------------------------------------------- #
# Code                                                                                            #
# ----------------------------------------------------------------------------------------------- #
"""
if not os.path.isfile('model_data/vae_test4'):
    print("VAE not found, run vae_training.py first")
    sys.exit()

# Load vae
vae64 = CarlaVae(latent_vector_size)
vae_agent64 = VaeAgent(vae64, seed=seed)
vae_agent64.load(filename='vae_test4')

# Run tests
print("Test 1")
base_env1 = CarlaEnv(ip=ip, port=port, image_size=(64, 64), n_actions=2, frameskip=4, framestack=1)
env1 = lambda: VaeWrapper(base_env1, vae_agent64.model, transform=transform)
model = TD3(env1, buffer_size=5000, tau=0.05, seed=seed)
start = timeit.default_timer()
model.train(training_timesteps)
stop = timeit.default_timer()
print("Training test 1 took {:.2f} seconds".format(stop - start))
model.save()
model.make_plot(filename="carla64_rew_loss_1")

print("Test 2")
base_env2 = CarlaEnv(ip=ip, port=port, image_size=(64, 64), n_actions=2, frameskip=4, framestack=1)
env2 = lambda: VaeWrapper(base_env2, vae_agent64.model, transform=transform)
model = TD3(env2, buffer_size=5000, tau=0.05, seed=seed+1)
start = timeit.default_timer()
model.train(training_timesteps)
stop = timeit.default_timer()
print("Training test 2 took {:.2f} seconds".format(stop - start))
model.save()
model.make_plot(filename="carla64_rew_loss_2")

print("Test 128im,64vec")
if not os.path.isfile('model_data/128im_64vec_vae'):
    print("VAE not found, run vae_training.py first")
    sys.exit()

# Load vae
vae_128im_64vec = CarlaVae128(64)
vae_agent_128im_64vec = VaeAgent(vae_128im_64vec, seed=seed)
vae_agent_128im_64vec.load(filename='128im_64vec_vae')

base_env_128im_64vec = CarlaEnv(ip=ip, port=port, image_size=(128, 128), n_actions=2, frameskip=4, 
                         framestack=1)
env_128im_64vec = lambda: VaeWrapper(base_env_128im_64vec, vae_agent_128im_64vec.model, transform=transform)
model = TD3(env_128im_64vec, buffer_size=5000, tau=0.05, seed=seed)
start = timeit.default_timer()
model.train(training_timesteps)
stop = timeit.default_timer()
print("Training test 128im,64vec took {:.2f} seconds".format(stop - start))
model.save()
model.make_plot(filename="rew_128im_64vec")

#####################################
print("Test 64im,64vec")
if not os.path.isfile('model_data/64im_64vec_vae'):
    print("VAE not found, run vae_training.py first")
    sys.exit()

# Load vae
vae_64im_64vec = CarlaVae(64)
vae_agent_64im_64vec = VaeAgent(vae_64im_64vec, seed=seed)
vae_agent_64im_64vec.load(filename='64im_64vec_vae')

base_env_64im_64vec = CarlaEnv(ip=ip, port=port, image_size=(64, 64), n_actions=2, frameskip=4, 
                         framestack=1)
env_64im_64vec = lambda: VaeWrapper(base_env_64im_64vec, vae_agent_64im_64vec.model, transform=transform)
model = TD3(env_64im_64vec, buffer_size=5000, tau=0.05, seed=seed)
start = timeit.default_timer()
model.train(training_timesteps)
stop = timeit.default_timer()
print("Training test 64im,64vec took {:.2f} seconds".format(stop - start))
model.save()
model.make_plot(filename="rew_64im_64vec")
"""

#####################################
print("Test 128im,32vec")
if not os.path.isfile('model_data/128im_32vec_vae'):
    print("VAE not found, run vae_training.py first")
    sys.exit()

# Load vae
vae_128im_32vec = CarlaVae128(32)
vae_agent_128im_32vec = VaeAgent(vae_128im_32vec, seed=seed)
vae_agent_128im_32vec.load(filename='128im_32vec_vae')

base_env_128im_32vec = CarlaEnv(ip=ip, port=port, image_size=(128, 128), n_actions=2, frameskip=4, 
                         framestack=1)
env_128im_32vec = lambda: VaeWrapper(base_env_128im_32vec, vae_agent_128im_32vec.model, transform=transform)
model = TD3(env_128im_32vec, buffer_size=5000, tau=0.05, seed=seed)
start = timeit.default_timer()
model.train(training_timesteps)
stop = timeit.default_timer()
print("Training test 128im,32vec took {:.2f} seconds".format(stop - start))
model.save()
model.make_plot(filename="rew_128im_32vec")