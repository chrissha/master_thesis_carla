"""
This file defines carla as a gym environment for use with OpenAI Gym.

Defines one car with one rgb senser in an empty city. Uses continuous action space.The 
functionality of the environment is specified in more detail on the individual methods.

Author: Christian S. Hådem
"""

# ----------------------------------------------------------------------------------------------- #
# Imports                                                                                         #
# ----------------------------------------------------------------------------------------------- #

# Built-in
import glob
import os
import sys

import random
import time
import queue
import itertools
import math


# Carla
sys.path.append(glob.glob('PythonAPI/carla/dist/carla-*3.5-linux-x86_64.egg')[0])
import carla

# Other third-party
import numpy as np
import cv2
import gym
from gym import spaces

# ----------------------------------------------------------------------------------------------- #
# Gym class                                                                                       #
# ----------------------------------------------------------------------------------------------- #
class CarlaEnv(gym.Env):
    """Custom OpenAI Gym environment implementing Carla simulator

    # TODO only create actors once and instead place it on a new spawnpoint on reset
    # TODO add max timestep before episode terminates (check how other environments does this)
    """
    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self, ip='localhost', port=2000, timeout=10., image_size=(200, 200), mp="Town01",
                 n_actions=3, fps=20, frameskip=4, framestack=4, max_timesteps=1000):
        """Initializes the carla driving environment

        Connects to the simulator using the given ip and port. Sets up different simulator and
        gym environment settings using the given parameters. Also sets the server to be
        synchronous mode with a given fps.

        params:
            ip:             The ip of the carla simulator server. Default is localhost
            port:           The port of the carla simulator server. Default is 2000 (same as server)
            timeout:        Timeout (in seconds) for connecting to the carla simulator server
            image_size:     Image width and height (in pixels) given as a tuple
            map:            What map to use.
            n_actions:      Number of actions used to control the vehicle. Actions inlcuded are:
                            (in order) throttle, steer, break.
            fps:            The fps to simulate in the simulation. The default 20fps will give a
                            fixed time delta of 1/20=0.05, which means that each frame will equal
                            to 0.05 seconds elapsed.
            frameskip:      Capture every 'frameskip' frame only. The default is set to 4 which
                            means that every 4th frame will be captured.  
            framestack:     How many frames to stack every observation. The default is set to 4
                            meaning that the observation will consist of the previous 4 frames.
            max_timesteps:  The maximum number of timesteps an episode will run for before 
                            restarting. Frames skipped with frameskip will not be added to the
                            max_timesteps counter.
            
        """
        assert 2 <= n_actions <= 3, "The number of actions must be either 2 or 3"
        assert fps >= 10, "Fps needs to be 10 at minimum to be in sync with the physics engine"
        assert frameskip > 0, "Frameskip can be at minimum 1"
        assert framestack > 0, "Framestack can be at minimum 1"
        assert max_timesteps > 0, "Max timesteps each episode has to be a positive number"

        # Assign parameters
        self.ip = ip
        self.port = port
        self.timeout = timeout
        self.image_width, self.image_height = image_size
        self.n_actions = n_actions
        self.frameskip = frameskip
        self.framestack = framestack
        self.max_timesteps = max_timesteps
        self.map = mp

        # Set up other variables
        self.actor_list = []
        self.done = True # Set to True to force env.reset()
        self.collided = True # Same as done
        self.episode = -1 # Initial reset sets it to 0
        self.image_queue = [] # Stores most recent observations
        self.current_stack = None
        self.current_frame = 0 # Current frame for the episode

        # Action space is in the format throttle, steer, brake
        # with limits 0 to 1, -1 to 1 and 0 to 1 respectively
        self.action_space = spaces.Box(low=-1, high=1,
            shape=(self.n_actions,), dtype=np.float32)
        # Observation space is a framestack x image_width x image_height array
        # with limits 0 to 255.
        self.observation_space = spaces.Box(low=0, high=255,
            shape=(self.framestack, self.image_width, self.image_height), dtype=np.uint8)

        # Define normalize function for mapping [-1, 1] to [0, 1]
        self.normalize = lambda x: (x + 1) / 2
        self.vec_normalize = np.vectorize(self.normalize)

        # Connect to the simulator
        self.client = carla.Client(self.ip, self.port)
        self.client.set_timeout(self.timeout)
        self.client.load_world(mp)

        self.world = self.client.get_world()
        self.blueprint_library = self.world.get_blueprint_library()
        self.world.set_weather(carla.WeatherParameters(sun_altitude_angle=90))

        # Tell the simulator to use fixed time steps
        settings = self.world.get_settings()
        if not settings.synchronous_mode:
            settings.fixed_delta_seconds = 1 / fps
            settings.synchronous_mode = True
            self.world.apply_settings(settings)


    # ------------------------------------------------------------------------------------------- #
    # Gym methods                                                                                 #
    # ------------------------------------------------------------------------------------------- #
    def step(self, action):
        """Steps the simulator

        The step function steps the environment one timestep by performing the given action. After
        the action is performed an observation is made from the camera sensors which is returned in
        grayscale. The episode terminates when the car crashes or the forward speed is over 
        the speed limit.
        
        params:
            action: The action to perform in the environment. The structure of an action is
                    a numpy array with three elements, throttle, steer and brake ranging from
                    -1 to 1.
        returns:
            obs:    An observation made by the rgb camera after the action has been performed.
                    Numpy array of values 0-255 
                    with shape: [framestack, image_width, image_height]
            reward: The reward given after performing the action. The reward given is the
                    forward speed.
            done:   True or False whether the episode is finished or not
            info:   Extra information about the environment after the action has been performed
                    given as a dictionary. Will always return an empty dictionary
        """
        if self.done:
            raise ValueError("Episode is finished or not reset. Run env.reset()")

        # Verify action shape
        assert self.action_space.contains(action), \
            "Invalid action {} of type {}".format(action, type(action))
        
        # Apply normalized action values to vehicle
        if self.n_actions == 2:
            v_throttle, v_steer = action
            v_throttle = self.normalize(v_throttle)
            v_control = carla.VehicleControl(float(v_throttle), float(v_steer), brake=0.,
                hand_brake=False, reverse=False, manual_gear_shift=False)
        else:
            v_throttle, v_steer, v_break = action
            v_throttle, v_break = self.vec_normalize([v_throttle, v_break])
            v_control = carla.VehicleControl(float(v_throttle), float(v_steer), float(v_break),
                hand_brake=False, reverse=False, manual_gear_shift=False)

        # Increment world
        for _ in range(self.frameskip):
            self.vehicle.apply_control(v_control) # Apply same action multiple times
            self.current_frame += 1 # Increment BEFORE skipping
            self.world.tick()

        # Get the observation
        self.current_stack = self.image_queue[-self.framestack:]
        if len(self.image_queue) > 16:
            self.image_queue.pop(0) # Remove the oldest image
        obs = np.array(self.current_stack)

        # Reward is calculated as speed
        vehicle_speed = self.vehicle.get_velocity()
        speed = np.sqrt(np.sum(np.square([vehicle_speed.x, vehicle_speed.y, vehicle_speed.z])))
        reward = speed

        """Check for termination"""
        done = False

        # Check if the speed is over the speed limit
        speed_limit = self.vehicle.get_speed_limit()
        if speed > speed_limit:
            done = True

        # Check if a collision has happened
        elif self.collided or self.crossed_line:
            done = True

        # Check if max timesteps has been reached
        elif self.current_frame // self.frameskip > self.max_timesteps:
            done = True

        return obs, reward, done, dict()


    def reset(self):
        """Resets the environment for a new episode

        The reset function resets the environment by destroying actors created in
        the previous episode and creates new ones. In total, a vehicle is created
        together with a rgb camera sensor at the front of the vehicle. 
        
        returns:
            np.array of the first observation by the camera sensor. 
            Size: [image_width, image_height, image_channels]
        """
        for actor in self.actor_list:
            if actor.is_alive:
                actor.destroy()
        
        # Reset variables
        self.actor_list = []
        self.episode += 1
        self.done = False
        self.collided = False
        self.crossed_line = False
        self.image_queue = []
        self.current_frame = 0

        # Set up the vehicle
        vehicle_bp = self.blueprint_library.find('vehicle.audi.a2')
        vehicle_bp.set_attribute('color', '255,255,255')
        assert vehicle_bp is not None, "VehicleBlueprint is None"
        vehicle_transform = random.choice(self.world.get_map().get_spawn_points())
        self.vehicle = self.custom_spawn_actor(vehicle_bp, vehicle_transform)
        self.actor_list.append(self.vehicle)

        # Set up camera_sensor  
        camera_bp = self.blueprint_library.find('sensor.camera.rgb')
        camera_bp.set_attribute('image_size_x', str(self.image_width))
        camera_bp.set_attribute('image_size_y', str(self.image_height))
        camera_bp.set_attribute('sensor_tick', str(0))
        relative_transform = carla.Transform(carla.Location(x=0.8, z=1.7))
        self.camera = self.custom_spawn_actor(camera_bp, relative_transform, 
                                                attach_to=self.vehicle)
        self.camera.listen(self.add_image)
        self.actor_list.append(self.camera)

        # Add a collision sensor
        collision_bp = self.blueprint_library.find('sensor.other.collision')
        self.collision_sensor = self.custom_spawn_actor(collision_bp, carla.Transform(),
                                                        attach_to=self.vehicle)
        self.collision_sensor.listen(self.collision_handler)
        self.actor_list.append(self.collision_sensor)

        # Add lane invasion sensor
        lane_invasion_bp = self.blueprint_library.find('sensor.other.lane_invasion')
        self.lane_invasion_sensor = self.custom_spawn_actor(lane_invasion_bp, carla.Transform(), 
                                                            attach_to=self.vehicle)
        self.lane_invasion_sensor.listen(self.lane_invasion_handler)
        self.actor_list.append(self.lane_invasion_sensor)

        # Change the spectator view to near the vehicle
        spectator = self.world.get_spectator()
        spectator_transform = vehicle_transform
        spectator_transform.location.z += 7.0 
        spectator_transform.rotation.pitch = -89 # 90 gives spectator bug when moving camera
        spectator.set_transform(spectator_transform)

        # Return the first observation
        for _ in range(self.framestack * self.frameskip): # Gather initial frames
            self.current_frame += 1 # Increment BEFORE ticking will ensure correct frameskip
            self.world.tick()
        while self.image_queue == []:
            self.world.tick()
        self.current_stack = self.image_queue[:self.framestack]
        return np.array(self.current_stack)


    def render(self, mode='human'):
        """Renders the camera sensor in a window

        Renders the most recent image recieved from the camera sensor to the given render mode.

        params:
            mode:   What method to render. 'human' creates a window with a display.
                    'array' returns a numpy array of the image in grayscale format.

        returns:
            An numpy array containing the most recent image in grayscale if the render mode
            is set to 'array'
        """
        assert self.current_stack is not None, \
            "render method has been called before reset. Make sure you run env.reset() first"

        if mode == 'human':
            cv2.imshow("Render", self.current_stack[-1])
            cv2.waitKey(1)
        elif mode == 'array':
            return self.current_stack


    def close(self):
        """Closes the environment

        Removes and resets what has been initialized by this client to
        reset the carla server state to its original state.
        """
        # Remove all existing actors (if any)
        for actor in self.actor_list:
            if actor.is_alive:
                actor.destroy()

        # Set world back to asynchronous so server doesn't wait for tick
        settings = self.world.get_settings()
        settings.synchronous_mode = False
        self.world.apply_settings(settings)


    def seed(self, seed):
        """Seeds the random generators for this environment.

        If a seed is given, the random generators used in this environment will use the given seed.
        Otherwise, random seeds will be used. Used for reproducability.

        params:
            seed:   The seed to use with random generators. 
        
        returns:
            A list of seeds used. An empty list if the seed is None.
        """
        random.seed(seed)
        np.random.seed(seed)
        self.action_space.seed(seed)

        return [seed for _ in range(3)] # 3 seeds used


    # ------------------------------------------------------------------------------------------- #
    # Helper methods                                                                              #
    # ------------------------------------------------------------------------------------------- #
    def convert_to_numpy(self, image):
        """Converts the image from the camera sensor to a numpy array.
        
        Converts an image from a sensor to numpy array of shape (WxHxC). Drops the alpha channel.
        """
        arr = np.array(image.raw_data).reshape(self.image_width, self.image_height, 4)
        return arr[:,:,:3]


    def custom_spawn_actor(self, blueprint, transform, attach_to=None, limit=math.inf):
        """Helper function for spawning an actor. Will try to spawn an actor indefinetly until the
        actor has been spawned or the limit parameter has been reached.

        params:
            blueprint:  The blueprint of the actor to spawn. Type carla.ActorBlueprint
            transform:  The transform which describes where to spawn the actor. 
                        Type carla.Transform
            limit:      How many times to try and spawn the actor. If None, tries indefinately

        returns:
            The actor on successful spawn. 

        throws:
            RunTimeError if limit was reached without spawn success.
        """
        for i in itertools.count():
            actor = self.world.try_spawn_actor(blueprint, transform, attach_to=attach_to)
            if actor is not None:
                return actor
            if i >= limit:
                break
            print("Failed to spawn actor, retrying...")
        raise RuntimeError('Failed to spawn actor within the given limit of {}'.format(limit))

        
    def add_image(self, image):
        """Callback function for rgb sensor tick
        
        params:
            image (carla.Image): A carla.Image object containing image information
        """
        if self.current_frame % self.frameskip == 0:
            self.image_queue.append(self.convert_to_numpy(image))


    def collision_handler(self, event):
        """Handles collisions detected by the collision sensor.

        params:
            event (carla.CollisionEvent): CollisionEvent object containing actor, other_actor and 
                                          normal_impulse
        """
        self.collided = True


    def lane_invasion_handler(self, event):
        """Callback function for handling a lane crossing

        params:
            event (carla.LaneInvasionEvent): A carla sensorData object containing information about
                                             the lane crossing.
        """
        self.crossed_line = True