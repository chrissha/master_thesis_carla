"""Script for changing the current map on the server"""
import sys
import glob

sys.path.append(glob.glob('PythonAPI/carla/dist/carla-*3.5-linux-x86_64.egg')[0])
import carla
import PyInquirer

# Hyperparams
ip = 'localhost'
port = 2000

client = carla.Client(ip, port)
client.set_timeout(10)
maps = client.get_available_maps()

maps = [m.split('/')[-1] for m in maps]

question = [
    {
        'type': 'list',
        'name': 'map',
        'message': 'Choose a map to load',
        'choices': maps,
    },
]

answer = PyInquirer.prompt(question)
new_map = answer['map']

client.load_world(new_map)
print("Map has been changed to {}".format(new_map))