"""Script for changing the current map on the server"""
import sys
import glob

sys.path.append(glob.glob('PythonAPI/carla/dist/carla-*3.5-linux-x86_64.egg')[0])
import carla

# Hyperparams
ip = 'localhost'
port = 2000

client = carla.Client(ip, port)
client.set_timeout(10)
world = client.get_world()
world.set_weather(carla.WeatherParameters(sun_altitude_angle=90))