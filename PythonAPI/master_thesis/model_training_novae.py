"""This file loads up a vae and starts training a TD3 model on Carla

Author: Christian S. Hådem
"""

# ----------------------------------------------------------------------------------------------- #
# Imports                                                                                         #
# ----------------------------------------------------------------------------------------------- #
import os, sys, timeit
from carla_cnn import Actor, Critic, Actor128, Critic128 # Must load torchvision before carla
from carla_env_temp import CarlaEnv
from vae_gym_wrapper import EmptyWrapper
from TD3_temp import TD3Temp

# ----------------------------------------------------------------------------------------------- #
# Parameters                                                                                      #
# ----------------------------------------------------------------------------------------------- #
ip = 'localhost'
port = 2000
seed = 1
training_timesteps = 100000

# ----------------------------------------------------------------------------------------------- #
# Code                                                                                            #
# ----------------------------------------------------------------------------------------------- #
"""
print("Test 1 no vae")
base_env = CarlaEnv(ip=ip, port=port, image_size=(64, 64), n_actions=2, frameskip=4, framestack=1)
env = lambda: EmptyWrapper(base_env)
model = TD3Temp(env, actor=Actor, critic=Critic, buffer_size=5000, tau=0.05, seed=seed)
start = timeit.default_timer()
model.train(training_timesteps)
stop = timeit.default_timer()
print("Training without vae test 1 took {:.2f} seconds".format(stop - start))
model.save()
model.make_plot(filename="carla_no_vae_rew_loss_1")
"""

print("Test 2 no vae (128im)")
base_env = CarlaEnv(ip=ip, port=port, image_size=(128, 128), n_actions=2, frameskip=4, framestack=1)
env = lambda: EmptyWrapper(base_env)
model = TD3Temp(env, actor=Actor128, critic=Critic128, buffer_size=5000, tau=0.05, seed=seed)
start = timeit.default_timer()
model.train(training_timesteps)
stop = timeit.default_timer()
print("Training without vae test 2 took {:.2f} seconds".format(stop - start))
model.save()
model.make_plot(filename="carla_no_vae_rew_loss_2")