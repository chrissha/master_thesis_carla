import torch
from torch import nn
from vae import BaseVae


class Actor(nn.Module):
    """The vae used for carla training"""
    def __init__(self, obs_space, act_space):
        super(Actor, self).__init__()

        self.fc_size = 2 * 2 * 256 # 1024
        
        # Encoder network                                                    # 3x64x64
        self.cnn1 = nn.Conv2d(3, 32, kernel_size=4, stride=2, padding=0)     # 32x31x31
        self.cnn2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0)    # 64x14x14
        self.cnn3 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=0)   # 128x6x6
        self.cnn4 = nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=0)  # 256x2x2        
        self.fc1 = nn.Linear(self.fc_size, 32)
        self.fc2 = nn.Linear(32, act_space)


    def forward(self, x):
        x = torch.relu(self.cnn1(x))
        x = torch.relu(self.cnn2(x))
        x = torch.relu(self.cnn3(x))
        x = torch.relu(self.cnn4(x))
        x = torch.flatten(x, start_dim=1)
        x = torch.relu(self.fc1(x))
        x = torch.tanh(self.fc2(x))
        return x.squeeze()


class Critic(nn.Module):
    """The vae used for carla training"""
    def __init__(self, obs_space, act_space):
        super(Critic, self).__init__()

        self.fc_size = 2 * 2 * 256 # 1024
        
        # Encoder network                                                    # 3x64x64
        self.cnn1 = nn.Conv2d(3, 32, kernel_size=4, stride=2, padding=0)     # 32x31x31
        self.cnn2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0)    # 64x14x14
        self.cnn3 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=0)   # 128x6x6
        self.cnn4 = nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=0)  # 256x2x2        
        self.fc1 = nn.Linear(self.fc_size + act_space, 32)
        self.fc2 = nn.Linear(32, 1)


    def forward(self, x, a):
        x = torch.relu(self.cnn1(x))
        x = torch.relu(self.cnn2(x))
        x = torch.relu(self.cnn3(x))
        x = torch.relu(self.cnn4(x))
        x = torch.flatten(x, start_dim=1)
        x = torch.cat([x, a], dim=1)
        x = torch.relu(self.fc1(x))
        x = self.fc2(x).squeeze()
        return x 


class Actor128(nn.Module):
    """The vae used for carla training"""
    def __init__(self, obs_space, act_space):
        super(Actor128, self).__init__()

        self.fc_size = 2 * 2 * 256 # 1024
        
        # Encoder network                                                    # 3x128x128
        self.cnn1 = nn.Conv2d(3, 16, kernel_size=4, stride=2, padding=0)     # 16x63x63
        self.cnn2 = nn.Conv2d(16, 32, kernel_size=4, stride=2, padding=0)    # 32x30x30
        self.cnn3 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0)    # 64x14x14
        self.cnn4 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=0)   # 128x6x6
        self.cnn5 = nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=0)  # 256x2x2     
        self.fc1 = nn.Linear(self.fc_size, 32)
        self.fc2 = nn.Linear(32, act_space)


    def forward(self, x):
        x = torch.relu(self.cnn1(x))
        x = torch.relu(self.cnn2(x))
        x = torch.relu(self.cnn3(x))
        x = torch.relu(self.cnn4(x))
        x = torch.relu(self.cnn5(x))
        x = torch.flatten(x, start_dim=1)
        x = torch.relu(self.fc1(x))
        x = torch.tanh(self.fc2(x))
        return x.squeeze()


class Critic128(nn.Module):
    """The vae used for carla training"""
    def __init__(self, obs_space, act_space):
        super(Critic128, self).__init__()

        self.fc_size = 2 * 2 * 256 # 1024
        
        # Encoder network                                                    # 3x128x128
        self.cnn1 = nn.Conv2d(3, 16, kernel_size=4, stride=2, padding=0)     # 16x63x63
        self.cnn2 = nn.Conv2d(16, 32, kernel_size=4, stride=2, padding=0)    # 32x30x30
        self.cnn3 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0)    # 64x14x14
        self.cnn4 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=0)   # 128x6x6
        self.cnn5 = nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=0)  # 256x2x2        
        self.fc1 = nn.Linear(self.fc_size + act_space, 32)
        self.fc2 = nn.Linear(32, 1)


    def forward(self, x, a):
        x = torch.relu(self.cnn1(x))
        x = torch.relu(self.cnn2(x))
        x = torch.relu(self.cnn3(x))
        x = torch.relu(self.cnn4(x))
        x = torch.relu(self.cnn5(x))
        x = torch.flatten(x, start_dim=1)
        x = torch.cat([x, a], dim=1)
        x = torch.relu(self.fc1(x))
        x = self.fc2(x).squeeze()
        return x 

