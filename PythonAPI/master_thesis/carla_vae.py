import torch
from torch import nn
from vae import BaseVae


class CarlaVae(BaseVae):
    """The vae used for carla training"""
    def __init__(self, latent_size):
        super(CarlaVae, self).__init__()

        self.latent_size = latent_size
        self.fc_size = 2 * 2 * 256 # 1024
        
        # Encoder network                                                       # 3x64x64
        self.en_cnn1 = nn.Conv2d(3, 32, kernel_size=4, stride=2, padding=0)     # 32x31x31
        self.en_cnn2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0)    # 64x14x14
        self.en_cnn3 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=0)   # 128x6x6
        self.en_cnn4 = nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=0)  # 256x2x2          
        self.en_mean = nn.Linear(self.fc_size, latent_size)
        self.en_std = nn.Linear(self.fc_size, latent_size)

        # Decoder network
        self.de_fc = nn.Linear(latent_size, self.fc_size)                                
        self.de_cnn1 = nn.ConvTranspose2d(1024, 128, kernel_size=5, stride=2, padding=0) 
        self.de_cnn2 = nn.ConvTranspose2d(128, 64, kernel_size=5, stride=2, padding=0)   
        self.de_cnn3 = nn.ConvTranspose2d(64, 32, kernel_size=6, stride=2, padding=0)   
        self.de_cnn4 = nn.ConvTranspose2d(32, 3, kernel_size=6, stride=2, padding=0)    


    def encoder(self, x):
        x = torch.relu(self.en_cnn1(x))
        x = torch.relu(self.en_cnn2(x))
        x = torch.relu(self.en_cnn3(x))
        x = torch.relu(self.en_cnn4(x))
        x = torch.flatten(x, start_dim=1)
        mean = self.en_mean(x)
        std = self.en_std(x)
        return mean, std


    def decoder(self, z):
        x = self.de_fc(z)
        x = x.view(-1, self.fc_size, 1, 1)
        x = torch.relu(self.de_cnn1(x))
        x = torch.relu(self.de_cnn2(x))
        x = torch.relu(self.de_cnn3(x))
        x = torch.sigmoid(self.de_cnn4(x))
        return x


class CarlaVae128(BaseVae):
    """The vae used for carla training"""
    def __init__(self, latent_size):
        super(CarlaVae128, self).__init__()

        self.latent_size = latent_size
        self.fc_size = 2 * 2 * 256 # 1024
        
        # Encoder network                                                       # 3x128x128
        self.en_cnn1 = nn.Conv2d(3, 16, kernel_size=4, stride=2, padding=0)     # 16x63x63
        self.en_cnn2 = nn.Conv2d(16, 32, kernel_size=4, stride=2, padding=0)    # 32x30x30
        self.en_cnn3 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0)    # 64x14x14
        self.en_cnn4 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=0)   # 128x6x6   
        self.en_cnn5 = nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=0)  # 256x2x2    
        self.en_mean = nn.Linear(self.fc_size, latent_size)
        self.en_std = nn.Linear(self.fc_size, latent_size)

        # Decoder network
        self.de_fc = nn.Linear(latent_size, self.fc_size)
        self.de_cnn1 = nn.ConvTranspose2d(1024, 128, kernel_size=6, stride=2, padding=0)
        self.de_cnn2 = nn.ConvTranspose2d(128, 64, kernel_size=4, stride=2, padding=0)
        self.de_cnn3 = nn.ConvTranspose2d(64, 32, kernel_size=4, stride=2, padding=0)
        self.de_cnn4 = nn.ConvTranspose2d(32, 16, kernel_size=5, stride=2, padding=0)
        self.de_cnn5 = nn.ConvTranspose2d(16, 3, kernel_size=4, stride=2, padding=0)


    def encoder(self, x):
        x = torch.relu(self.en_cnn1(x))
        x = torch.relu(self.en_cnn2(x))
        x = torch.relu(self.en_cnn3(x))
        x = torch.relu(self.en_cnn4(x))
        x = torch.relu(self.en_cnn5(x))
        x = torch.flatten(x, start_dim=1)
        mean = self.en_mean(x)
        std = self.en_std(x)
        return mean, std


    def decoder(self, z):
        x = self.de_fc(z)
        x = x.view(-1, self.fc_size, 1, 1)
        x = torch.relu(self.de_cnn1(x))
        x = torch.relu(self.de_cnn2(x))
        x = torch.relu(self.de_cnn3(x))
        x = torch.relu(self.de_cnn4(x))
        x = torch.sigmoid(self.de_cnn5(x))
        return x