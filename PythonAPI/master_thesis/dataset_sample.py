import os, sys
import torch
import numpy as np
import matplotlib.pyplot as plt
from torchvision import transforms, utils
from carla_dataset import CarlaDataset

latent_vector_size = 32
transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.ToTensor()
])

if not os.path.exists('model_data/dataset128'):
    print("Dataset is missing")
    sys.exit()

dataset = CarlaDataset(transform=transform)
dataset.load(filename="dataset128")
indicies = np.random.randint(low=0, high=len(dataset), size=3)

ims = []
for i in indicies:
    im, _ = dataset[i]
    ims.append(im)
ims = torch.stack(ims)

grid = utils.make_grid(ims, nrow=3).permute(1, 2, 0)
plt.imshow(grid.numpy())
plt.axis('off')
plt.savefig("dataset_example")