import os, sys, timeit
from carla_cnn import Actor, Critic # Must load torchvision before carla
from carla_env_temp import CarlaEnv
from vae_gym_wrapper import EmptyWrapper
from TD3 import TD3

env = CarlaEnv(ip='localhost', port=2000, image_size=(64, 64), n_actions=2, 
               frameskip=4, framestack=1)

obs = env.reset()
print(obs.shape)