"""This file collects a dataset (or loads existing one) and trains a vae (unless vae file exists)

Author: Christian S. Hådem
"""

# ----------------------------------------------------------------------------------------------- #
# Imports                                                                                         #
# ----------------------------------------------------------------------------------------------- #
import os, sys, timeit
import numpy as np
from torchvision import transforms
from carla_dataset import CarlaDataset
from carla_vae import CarlaVae, CarlaVae128
from carla_env import CarlaEnv
from vae import VaeAgent

# ----------------------------------------------------------------------------------------------- #
# Parameters                                                                                      #
# ----------------------------------------------------------------------------------------------- #
ip = 'localhost'
port = 2000
seed = 0
vae_dataset_size = 10000
latent_vector_size = 32
transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.ToTensor()
])


# ----------------------------------------------------------------------------------------------- #
# Code                                                                                            #
# ----------------------------------------------------------------------------------------------- #

# Look for datasets, else start collecting missing ones
dataset64 = CarlaDataset(transform=transform)
dataset128 = CarlaDataset(transform=transform)

if not os.path.isfile('model_data/dataset64'):
    print("Dataset not found, collecting...")
    temp_env = CarlaEnv(ip=ip, port=port, image_size=(64, 64), n_actions=2, frameskip=1, 
                        framestack=1)
    temp_env.seed(seed)
    dataset64.collect(temp_env, size=vae_dataset_size)
    dataset64.save(filename='dataset64')
    temp_env.close()
else:
    print("Dataset found, loading dataset")
    dataset64.load(filename='dataset64')

if not os.path.isfile('model_data/dataset128'):
    print("Dataset not found, collecting...")
    temp_env = CarlaEnv(ip=ip, port=port, image_size=(128, 128), n_actions=2, frameskip=1, 
                        framestack=1)
    temp_env.seed(seed)
    dataset128.collect(temp_env, size=vae_dataset_size)
    dataset128.save(filename='dataset128')
    temp_env.close()
else:
    print("Dataset found, loading dataset")
    dataset128.load(filename='dataset128')

np.random.seed(seed)
indicies = np.random.randint(low=0, high=len(dataset64) - 1, size=10)

"""
# VAE Test 1
print("VAE Test 1")
vae = CarlaVae(32)
vae_agent = VaeAgent(vae, epochs=10, batch_size=256, lr=0.001)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training test 1 took {:.2f} seconds".format(stop - start))
vae_agent.save(filename='vae_64')
vae_agent.print_result(dataset64, indicies, to_file=True, filename='vae_test_1')
vae_agent.plot_loss(to_file=True, filename='vae_test_1_loss')

# VAE Test 2
print("VAE Test 2")
vae = CarlaVae(latent_vector_size)
vae_agent = VaeAgent(vae, epochs=10, batch_size=256, lr=0.01)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training test 2 took {:.2f} seconds".format(stop - start))
vae_agent.print_result(dataset64, indicies, to_file=True, filename='vae_test_2')
vae_agent.plot_loss(to_file=True, filename='vae_test_2_loss')

# VAE Test 3
print("VAE Test 3")
vae = CarlaVae(latent_vector_size)
vae_agent = VaeAgent(vae, epochs=10, batch_size=256, lr=0.0001)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training test 3 took {:.2f} seconds".format(stop - start))
vae_agent.print_result(dataset64, indicies, to_file=True, filename='vae_test_3')
vae_agent.plot_loss(to_file=True, filename='vae_test_3_loss')

# VAE Test 4
print("VAE Test 4")
vae = CarlaVae(latent_vector_size)
vae_agent = VaeAgent(vae, epochs=10, batch_size=64, lr=0.001)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training test 4 took {:.2f} seconds".format(stop - start))
vae_agent.save(filename="vae_test4")
vae_agent.print_result(dataset64, indicies, to_file=True, filename='vae_test_4')
vae_agent.plot_loss(to_file=True, filename='vae_test_4_loss')

# VAE Test 5
print("VAE Test 5")
vae = CarlaVae(latent_vector_size)
vae_agent = VaeAgent(vae, epochs=25, batch_size=64, lr=0.001)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training test 5 took {:.2f} seconds".format(stop - start))
vae_agent.print_result(dataset64, indicies, to_file=True, filename='vae_test_5')
vae_agent.plot_loss(to_file=True, filename='vae_test_5_loss')

# VAE Test 6
print("VAE Test 6")
vae = CarlaVae(latent_vector_size)
vae_agent = VaeAgent(vae, epochs=10, batch_size=64, lr=0.005)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training test 6 took {:.2f} seconds".format(stop - start))
vae_agent.print_result(dataset64, indicies, to_file=True, filename='vae_test_6')
vae_agent.plot_loss(to_file=True, filename='vae_test_6_loss')

# VAE Test 7'
print("VAE Test 7")
vae = CarlaVae(latent_vector_size)
vae_agent = VaeAgent(vae, epochs=10, batch_size=256, lr=0.005)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training test 7 took {:.2f} seconds".format(stop - start))
vae_agent.print_result(dataset64, indicies, to_file=True, filename='vae_test_7')
vae_agent.plot_loss(to_file=True, filename='vae_test_7_loss')

# VAE Test 8
print("VAE Test 8")
vae = CarlaVae(latent_vector_size)
vae_agent = VaeAgent(vae, epochs=1, batch_size=64, lr=0.001, seed=seed)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training test 8 took {:.2f} seconds".format(stop - start))
vae_agent.print_result(dataset64, indicies, to_file=True, filename='vae_test_8')
vae_agent.plot_loss(to_file=True, filename='vae_test_8_loss')

"""
print("VAE 64-image-size, 32-latent-vector-size")
vae = CarlaVae(32)
vae_agent = VaeAgent(vae, epochs=10, batch_size=256, lr=0.001, seed=seed)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training 64 image size, 32-latent-vector size took {:.2f} seconds".format(stop - start))
vae_agent.save(filename='64im_32vec_vae')
vae_agent.print_result(dataset64, indicies, to_file=True, filename='64im_32vec')
vae_agent.plot_loss(to_file=True, filename='64im_32vec_loss')

print("VAE 128-image-size, 32-latent-vector-size")
vae = CarlaVae128(32)
vae_agent = VaeAgent(vae, epochs=10, batch_size=256, lr=0.001, seed=seed)
start = timeit.default_timer()
vae_agent.train(dataset128)
stop = timeit.default_timer()
print("Training 128 image size, 32-latent-vector size took {:.2f} seconds".format(stop - start))
vae_agent.save(filename='128im_32vec_vae')
vae_agent.print_result(dataset128, indicies, to_file=True, filename='128im_32vec')
vae_agent.plot_loss(to_file=True, filename='128im_32vec_loss')

print("VAE 64-image-size, 64-latent-vector-size")
vae = CarlaVae(64)
vae_agent = VaeAgent(vae, epochs=10, batch_size=256, lr=0.001, seed=seed)
start = timeit.default_timer()
vae_agent.train(dataset64)
stop = timeit.default_timer()
print("Training 64 image size, 64-latent-vector size took {:.2f} seconds".format(stop - start))
vae_agent.save(filename='64im_64vec_vae')
vae_agent.print_result(dataset64, indicies, to_file=True, filename='64im_64vec')
vae_agent.plot_loss(to_file=True, filename='64im_64vec_loss')

print("VAE 128-image-size, 64-latent-vector-size")
vae = CarlaVae128(64)
vae_agent = VaeAgent(vae, epochs=10, batch_size=256, lr=0.001, seed=seed)
start = timeit.default_timer()
vae_agent.train(dataset128)
stop = timeit.default_timer()
print("Training 128 image size, 64-latent-vector size took {:.2f} seconds".format(stop - start))
vae_agent.save(filename='128im_64vec_vae')
vae_agent.print_result(dataset128, indicies, to_file=True, filename='128im_64vec')
vae_agent.plot_loss(to_file=True, filename='128im_64vec_loss')