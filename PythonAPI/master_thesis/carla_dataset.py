import os
import pickle
import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms

class CarlaDataset(Dataset):
    """Class to create a dataset from a gym environment"""
    def __init__(self, transform=transforms.ToTensor()):
        """Initializes the dataset
        
        Initializes the dataset by acting randomly in the given environment until the desired size
        of the dataset has been reached.
        
        params:
            env (gym.Env): The gym environment to collect dataset from
            size (int): The size of the dataset
        """
        self.transform = transform
        self.dataset = []
        

    def __len__(self):
        """Returns the length of the dataset"""
        return len(self.dataset)


    def __getitem__(self, index):
        """Returns the data at the given index"""
        return self.transform(self.dataset[index]), 0 # Target is unused

    
    def collect(self, env, size=10000):
        """Runs the environment and appends the given size to the dataset"""
        dataset = []
        obs = env.reset()
        for _ in range(size):
            act = env.action_space.sample()
            obs, _, done, _ = env.step(act)
            dataset.append(obs)

            if len(dataset) % 100 == 0:
                print("Dataset size: {}".format(len(dataset)))
            
            if done:
                obs = env.reset()
                continue

        env.close()
        self.dataset = np.array(dataset).squeeze()



    def convert(self):
        """Converts the dataset to be a single numpy array instead of a list. Adding more samples 
        to the dataset can not be done after conversion"""
        self.dataset = np.array(self.dataset)


    def save(self, filename='dataset'):
        """Saves the dataset to file"""
        if len(self) == 0:
            print("No data in this dataset, cannot save.")
            return
        
        os.makedirs('model_data', exist_ok=True)
        with open('model_data/' + filename, 'wb') as f:
            pickle.dump(self.dataset, f)


    def load(self, filename='dataset'):
        """Load a dataset from file"""
        if not os.path.isfile('model_data/' + filename):
            print("File not found")
            return

        with open('model_data/' + filename, 'rb') as f:
            self.dataset = pickle.load(f)