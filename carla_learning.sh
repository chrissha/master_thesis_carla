#!/usr/bin/env bash

if [[ $1 == "start" ]]; then
    source env/bin/activate
    export DISPLAY=
    nohup ./CarlaUE4.sh --quality-mode=Low -opengl &>/dev/null &
    python PythonAPI/master_thesis/training.py
elif [[ $1 == "exit" ]]; then
    ID=pgrep CarlaUE4-Linux-
    kill $ID
fi